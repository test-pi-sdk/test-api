﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using User.Models;

namespace WebApplication3.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserAuthController : ControllerBase
    {
        private readonly UserContext _context;

        private readonly HttpClient _httpClient;

        public UserAuthController(UserContext context)
        {
            _context = context;
            _httpClient = new HttpClient();
        }

        // GET: api/user
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserAuth>>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }

        // GET: api/user/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserAuth>> GetUserAuth(string id)
        {
            var userAuth = await _context.Users.FindAsync(id);

            if (userAuth == null)
            {
                return NotFound();
            }

            return userAuth;
        }

        // PUT: api/user/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserAuth(string id, UserAuth userAuth)
        {
            if (id != userAuth.uid)
            {
                return BadRequest();
            }

            _context.Entry(userAuth).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserAuthExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/user
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<UserAuth>> PostUserAuth(UserAuth userAuth)
        {
            _context.Users.Add(userAuth);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserAuthExists(userAuth.uid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUserAuth", new { id = userAuth.uid }, userAuth);
        }

        // DELETE: api/user/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserAuth(string id)
        {
            var userAuth = await _context.Users.FindAsync(id);
            if (userAuth == null)
            {
                return NotFound();
            }

            _context.Users.Remove(userAuth);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserAuthExists(string id)
        {
            return _context.Users.Any(e => e.uid == id);
        }

        [HttpPost]
        [Route("signin")]
        public async Task<ActionResult<UserAuth>> SignIn([FromBody] UserAuth auth)
        {
            try
            {
                if (auth == null)
                {
                    return BadRequest();
                }
                try
                {
                    _httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + auth.accessToken);
                    using (var response = await _httpClient.GetAsync("https://api.minepi.com/v2/me"))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        var data = JsonConvert.DeserializeObject<UserAuth>(apiResponse);
                        // Access DB here
                        //return Ok(new { message = "User signed in" });
                        return Ok(data);

                    }
                }
                catch
                {
                    return StatusCode(500, new { message = "Invalid access token" });
                }
            }
            catch
            { throw; }
        }
    }
}

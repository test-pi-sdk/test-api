﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection.Metadata;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Payment.Models;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    [Route("api/payments")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentContext _context;
        private readonly HttpClient _httpClient;

        public PaymentController(PaymentContext context)
        {
            _context = context;
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Add("Authorization", $"Key iq8q7x4mfflu3knquylqnte6ocejw1ez6zfuidygysrihiolay3qqm2hwo6qdcmw");
        }

        // GET: api/Payment
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PaymentDTO>>> GetPayments()
        {
            return await _context.Payments.ToListAsync();
        }

        // GET: api/Payment/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PaymentDTO>> GetPaymentDTO(string id)
        {
            var paymentDTO = await _context.Payments.FindAsync(id);

            if (paymentDTO == null)
            {
                return NotFound();
            }

            return paymentDTO;
        }

        // PUT: api/Payment/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPaymentDTO(string id, PaymentDTO paymentDTO)
        {
            if (id != paymentDTO.identifier)
            {
                return BadRequest();
            }

            _context.Entry(paymentDTO).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaymentDTOExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Payment
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PaymentDTO>> PostPaymentDTO(PaymentDTO paymentDTO)
        {
            _context.Payments.Add(paymentDTO);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PaymentDTOExists(paymentDTO.identifier))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPaymentDTO", new { id = paymentDTO.identifier }, paymentDTO);
        }

        // DELETE: api/Payment/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePaymentDTO(string id)
        {
            var paymentDTO = await _context.Payments.FindAsync(id);
            if (paymentDTO == null)
            {
                return NotFound();
            }

            _context.Payments.Remove(paymentDTO);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PaymentDTOExists(string id)
        {
            return _context.Payments.Any(e => e.identifier == id);
        }

        [HttpPost]
        [Route("incomplete")]
        public async Task<ActionResult<PaymentDTO>> Incomplete([FromBody] PaymentDTO payment)
        {
            try
            {
                if (payment == null)
                {
                    return BadRequest();
                }
                try
                {
                    /* 
                    implement your logic here
                    e.g. verifying the payment, delivering the item to the user, etc...
                    below is a naive example
                    */

                    var paymentId = payment.identifier;
                    var txid = payment.transaction.txid;
                    var txUrl = payment.transaction._link;

                    // find the incomplete order
                    /*
                    access to DB and query
                    const order = await orderCollection.findOne({ pi_payment_id: paymentId });
                    if (!order) {
                      return StatusCode(400, new { message: "Order not found" });
                    }
                    */

                    // check the transaction on the Pi blockchain
                    using (var response = await _httpClient.GetAsync(txUrl))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        var data = JsonConvert.DeserializeObject<dynamic>(apiResponse);
                        if (data == null)
                        {
                            return BadRequest(data);
                        }
                        //var paymentIdOnBlock = data.memo;
                        // and check other data as well e.g. amount
                        //if (paymentIdOnBlock !== order.pi_payment_id)
                        //{
                        //    return res.status(400).json({ message: "PaymentDTO id doesn't match." });
                        //}
                        // mark the order as paid
                        //await orderCollection.updateOne({ pi_payment_id: paymentId }, { $set: { txid, paid: true } });

                        // let Pi Servers know that the payment is completed

                        var content = new StringContent(JsonConvert.SerializeObject(new { txid }), Encoding.UTF8, "application/json");
                        var res = await _httpClient.PostAsync($"https://api.minepi.com/v2/payments/{paymentId}/complete", content);

                        if (res.IsSuccessStatusCode)
                        {
                            var responseContent = await res.Content.ReadAsStringAsync();
                            return Ok(new { message = $"Handled the incomplete payment {paymentId}" });

                        }
                        else
                        {
                            var responseContent = await res.Content.ReadAsStringAsync();
                            return StatusCode(500, new { message = "Incomplete API Error" });
                        }
                    }
                }
                catch
                {
                    return StatusCode(500, new { message = "Catch Error" });
                }
            }
            catch { throw; }
        }

        [HttpPost]
        [Route("complete")]
        public async Task<ActionResult<PaymentDTO>> Complete([FromBody] BodyPayment payment)
        {
            try
            {
                var paymentId = payment.paymentId;
                var txid = payment.txid;

                /* 
                 implement your logic here
                 e.g. verify the transaction, deliver the item to the user, etc...
                */

                //await orderCollection.updateOne({ pi_payment_id: paymentId }, { $set: { txid: txid, paid: true } });
                
                var content = new StringContent(JsonConvert.SerializeObject(new { txid }), Encoding.UTF8, "application/json");
                var res = await _httpClient.PostAsync($"https://api.minepi.com/v2/payments/{paymentId}/complete", content);
                if (res.IsSuccessStatusCode)
                {
                    var responseContent = await res.Content.ReadAsStringAsync();
                    return Ok(new { message = $"Completed the payment {paymentId}" });

                }
                else
                {
                    return StatusCode(500, new { message = "Complete API Error" });
                }

            }
            catch { throw; }
        }

        [HttpPost]
        [Route("approve")]
        public async Task<ActionResult<PaymentDTO>> Approve([FromBody] BodyPayment paymentId)
        {
            try
            {
                if (paymentId == null)
                {
                    return BadRequest();
                }
                using (var currentPayment = await _httpClient.GetAsync($"https://api.minepi.com/v2/payments/{paymentId.paymentId}"))
                {
                    string apiResponse = await currentPayment.Content.ReadAsStringAsync();
                    var currentPaymentData = JsonConvert.DeserializeObject<dynamic>(apiResponse);

                    /*
                     * access to DB
                     * implement your logic here
                     * e.g. creating an order record, reserve an item if the quantity is limited, etc...
                     * await orderCollection.insertOne({
                     *    pi_payment_id: paymentId,
                     *    product_id: currentPaymentData.data.metadata.productId,
                     *    user: req.session.currentUser.uid,
                     *    txid: null,
                     *    paid: false,
                     *    cancelled: false,
                     *    created_at: new Date()
                     *  });
                     */

                    // let Pi Servers know that you're ready
                    var request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Post,
                        RequestUri = new Uri($"https://api.minepi.com/v2/payments/{paymentId.paymentId}/approve")
                    };
                    var res = await _httpClient.SendAsync(request);
                    if (res.IsSuccessStatusCode)
                    {
                        var responseContent = await res.Content.ReadAsStringAsync();
                        return Ok(new { message = $"Approved the payment {paymentId.paymentId}" });

                    }
                    else
                    {
                        return StatusCode(500, new { message = "Approve API Error" });
                    }
                }
            }
            catch { throw; }
        }
    }
}

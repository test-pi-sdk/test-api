﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Models
{
    public class Transaction
    {
        [Key]
        public string txid { get; set; }
        public bool verified { get; set; }
        public string _link { get; set; }
    }
}

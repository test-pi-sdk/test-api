﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace User.Models
{
    public class UserAuth
    {
        [Key]
        public string uid { get; set; }
        public string accessToken { get; set; }
        public string username { get; set; }

    }
}

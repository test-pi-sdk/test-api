﻿namespace Payment.Models
{
    public class PaymentStatus
    {
        public string Id { get; set; }
        public bool developer_approved { get; set; }
        public bool transaction_verified { get; set; }
        public bool developer_completed { get; set; }
        public bool cancelled { get; set; }
        public bool user_cancelled { get; set; }
    }
}

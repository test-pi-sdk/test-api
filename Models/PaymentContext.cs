﻿using Microsoft.EntityFrameworkCore;

namespace Payment.Models
{

        public class PaymentContext : DbContext
        {
            public PaymentContext(DbContextOptions<PaymentContext> options)
              : base(options)
            {
            }

            public DbSet<PaymentDTO> Payments { get; set; } = null!;
        }
}

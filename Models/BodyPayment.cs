﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication3.Models
{
    public class BodyPayment
    {
        [Key]
        public string paymentId { get; set; }
        public string txid { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Payment.Models
{
    public class PaymentDTO
    {
        [Key]
        public string identifier { get; set; }
        public string uid { get; set; }
        public float amount { get; set; }
        public string memo { get; set; }
        public string metadata { get; set; }
        public string createdAt { get; set; }
        //public PaymentStatus status { get; set; }
        public Transaction transaction { get; set; }
    }
}

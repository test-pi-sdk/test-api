﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace User.Models
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions<UserContext> options)
          : base(options)
        {
        }

        public DbSet<UserAuth> Users { get; set; } = null!;
    }
}

using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using Payment.Models;
using User.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<UserContext>(opt =>
    opt.UseInMemoryDatabase("User"));
builder.Services.AddDbContext<PaymentContext>(opt =>
    opt.UseInMemoryDatabase("Payment"));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
//builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    //app.UseSwagger();
    //app.UseSwaggerUI();
}

app.UseCors(builder =>
{
    builder
        .AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader();
});

app.UseHttpsRedirection();

app.UseAuthorization();

//app.Use(async (context, next) =>
//{
//    //context.Request.Headers.Add("Authorization", "Key " + "iq8q7x4mfflu3knquylqnte6ocejw1ez6zfuidygysrihiolay3qqm2hwo6qdcmw");
//    await next.Invoke();
//});

app.MapControllers();

app.Run();
